{% for server, args in pillar.get('servers', {}).items() %}
{{server}}_IIS-WebServerRole:
  win_servermanager.installed:
    - recurse: True
    - name: Web-Server
{% endfor %}