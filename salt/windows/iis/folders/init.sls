{% for server, args in pillar.get('servers', {}).items() %}
{{server}}_folders_test:
  file.exists:
    - names:
      - C:\Websites\{{ args['site']}}\wwwroot
      - C:\Websites\{{ args['site']}}\log

{{server}}_folders_create:
  cmd.run:
    - name: New-Item -Path 'C:\Websites\{{ args['site']}}\wwwroot','C:\Websites\{{ args['site']}}\log' -ItemType Directory
    - shell: powershell
    - onfail:
      - {{server}}_folders_test
{% endfor %}