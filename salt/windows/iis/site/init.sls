include:
  - windows.iis.folders
  - windows.iis.apppool

{% for server, args in pillar.get('servers', {}).items() %}
{{server}}_site:
  win_iis.deployed:
    - name: {{ args['site']}}
    - sourcepath: C:\Websites\{{ args['site']}}\wwwroot
    - apppool: {{ args['site']}}
    - protocol: https
{% endfor %}