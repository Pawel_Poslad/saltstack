{% for server, args in pillar.get('servers', {}).items() %}
{{server}}_apppool:
  win_iis.create_apppool:
    - name: {{ args['site']}}
{% endfor %}